clear all
rng(sum('anson'))
colormap cool

% volume,cx,cy,cz,Ix,Iy,Iz
scaled_data = readmatrix('C:\Coding\report-2\scaled_dataset.csv');
scaled_data = scaled_data(:,2:end);
[coeff,score,latent] = pca(scaled_data);


biplot(coeff(:,1:3),'scores',score(:,1:3),'varlabels',{'c_x','c_y','c_z','I_x','I_y','I_z'});
title("Center of Gravity Biplot")
xlabel("Center of Mass x")
ylabel("Center of Mass y")
zlabel("Center of Mass z")

biplot(coeff(:,4:6),'scores',score(:,4:6),'varlabels',{'c_x','c_y','c_z','I_x','I_y','I_z'});
title("Inertia Biplot")
xlabel("I_x")
ylabel("I_y")
zlabel("I_z")


data = readmatrix('C:\Coding\report-2\dataset.csv');
inertia = data(:,end-2:end);
inertia = rmoutliers(inertia, 'percentile', [0 99]); % remove single huge outlier
[IDX, C] = kmeans(inertia,3);
histcounts(IDX)
scatter3(inertia(:,1), inertia(:,2), inertia(:,3), [], IDX, 'filled')
inertia = inertia(IDX == 1,:);
[IDX, C] = kmeans(inertia,3);
histcounts(IDX)

scatter3(inertia(IDX == 1,1), inertia(IDX == 1,2), inertia(IDX == 1,3), 'filled')
hold on
for i = 2:max(IDX)
scatter3(inertia(IDX == i,1), inertia(IDX == i,2), inertia(IDX == i,3), 'filled')
end

legend("Cluster 1", "Cluster 2", "Cluster 3", "Cluster 4")

title("Satellite Parts Moments of Inertia (mm^4)")
xlabel("I_x")
ylabel("I_y")
zlabel("I_z")