using FileIO
using MeshIO

using stlProcess

using CSV
using DataFrames
# using SplomPlots
using ProgressBars

using LinearAlgebra

# local path to https://gitlab.com/orbital-debris-research/fake-satellite-dataset
dataset_path = raw"C:\Coding\fake-satellite-dataset"

folders = ["1_5U", "assembly1", "cubesat"]

# for folder in folders
# dataset_path * "\\" * folder |> 

# end
df = DataFrame(; volume=Float64[], cx=Float64[], cy=Float64[], cz=Float64[], Ix=Float64[], Iy=Float64[], Iz=Float64[])
println("=== Scaled Dataset ===")
for path in dataset_path * "\\" .* folders
    println("Processing Path: ", path)
    Threads.@threads for file in ProgressBar(readdir(path))
        stl = load(path * "\\" * file)
        props = get_mass_properties(stl; scale=find_scale(stl))

        cx, xy, xz = props.center_of_gravity
        Ix, Iy, Iz = eigvals(props.inertia)

        push!(df, [props.volume, cx, xy, xz, Ix, Iy, Iz])
    end
    println("")
end

println(describe(df))
CSV.write("scaled_dataset.csv", df)

df = DataFrame(; volume=Float64[], cx=Float64[], cy=Float64[], cz=Float64[], Ix=Float64[], Iy=Float64[], Iz=Float64[])
println("=== Raw Dataset ===")
for path in dataset_path * "\\" .* folders
    println("Processing Path: ", path)
    Threads.@threads for file in ProgressBar(readdir(path))
        stl = load(path * "\\" * file)
        props = get_mass_properties(stl)

        cx, xy, xz = props.center_of_gravity
        Ix, Iy, Iz = eigvals(props.inertia)

        push!(df, [props.volume, cx, xy, xz, Ix, Iy, Iz])
    end
    println("")
end

println(describe(df))
CSV.write("dataset.csv", df)

# splom(df[df.Iy .< 1.0,:])
